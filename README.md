# Ansible Role: Infracost

[![pipeline status](https://gitlab.com/enmanuelmoreira/ansible-role-infracost/badges/main/pipeline.svg)](https://gitlab.com/enmanuelmoreira/ansible-role-infracost/-/commits/main) 

This role installs [Infracost](https://github.com/accurics/infracost/) binary on any supported host.

## Requirements

github3.py >= 1.0.0a3

It can be installed from pip:

```bash
pip install github3.py
```

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    infracost_version: latest # v0.9.16 if you want a specific version
    infracost_arch: amd64 # amd64 or arm64
    setup_dir: /tmp
    infracost_bin_path: /usr/local/bin/infracost
    infracost_repo_path: https://github.com/infracost/infracost/releases/download

This role can install the latest or a specific version. See [available Infracost releases](https://github.com/infracost/infracost/releases/) and change this variable accordingly.

    infracost_version: latest # v0.9.16 if you want a specific version 

The path of the Infracost repository.

    infracost_repo_path: https://github.com/infracost/infracost/releases/download

The location where the Infracost binary will be installed.

    infracost_bin_path: /usr/local/bin/infracost

Infracost supports amd64 and arm64 CPU architectures, just change for the main architecture of your CPU.

    infracost_arch: amd64 # amd64 or arm64

Infracost needs a GitHub token so it can be downloaded. You must use the **github_token** variable in `vars/main.yml` in order to install infracost. However, this value can be empty until GitHub reaches the maximum number of anonymous connections.

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: infracost

## License

MIT / BSD
